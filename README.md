Vintage Story simple rowboat
=====================================
Basic rowboat that fits two players, WASD controls like Mineman:
- `W`/`S`: accelerate forward/reverse
- `A`/`D`: turn left/right
- [Right click] to place boat
- [Sneak + right click] to pick up boat

By default option `RequireOar = true` so players need a `Rowboat oar`
item **in both hands** to actually use the boat.
This can be disabled in mod config.
Both front and rear seat can also control the boat.
When two players row boat together, they can move faster than
just a single player (can adjust in mod config).

Boats should be able to move up/down water using default ingame 
entity water physics.

Client rowboat just sends movement control packets to server.
Server handles actual rowboat movement. 

`>but the rowing is wrong and boat backwards`  
yes I've rowed b4 & know the animations r incorrect, but its
shitty gameplay to row backwards even if correct irl.
deal with it :^)  
if someone sends me better anims ill add 

Config Options
=====================================
| Basic config                 | Description                       | Default |
|:-----------------------------|:----------------------------------|:-------:|
| RequireOar                   | Reqire oar item to control boat   |   true  |
| AllowFrontSeatToControlBoat  | Allow front seat to control       |   true  |
| AllowRearSeatToControlBoat   | Allow rear seat to control        |   true  |

To make it a mineman style boat, set:
- `RequireOar=false`
- `AllowFrontSeatToControlBoat=true`
- `AllowRearSeatToControlBoat=false`

| Boat mechanics config        | Description                       | Default |
|:-----------------------------|:----------------------------------|:-------:|
| MaxForwardSpeedSingle        | Max forward speed, 1 player row   |   0.15  |
| MaxReverseSpeedSingle        | Max reverse speed, 1 player row   |  -0.05  |
| MaxForwardSpeedDouble        | Max forward speed, 2 players row  |   0.18  |
| MaxReverseSpeedDouble        | Max reverse speed, 2 players row  |  -0.1   |
| MaxForwardSpeedOnLand        | Max forward speed on land         |   0.02  |
| MaxReverseSpeedOnLand        | Max reverse speed on land         |  -0.02  |
| ForwardAcceleration          | Forward acceleration per player   |   0.005 |
| ReverseAcceleration          | Reverse acceleration per player   |   0.005 |
| DragDecelerationFactor       | Multiplicative deceleration       |   0.8   |
| ClampSpeedToZeroThreshold    | Min speed before clamping to 0    |   0.1   |
| TurnAcceleration             | Turning acceleration per player   |   0.002 |
| DragTurningFactor            | Turning deceleration              |   0.6   |
| MaxAngularVelocitySingle     | Max turning speed, 1 player row   |   0.05  |
| MaxAngularVelocityDouble     | Max turning speed, 2 player row   |   0.08  |

| Advanced                     | Description                             | Default |
|:-----------------------------|:----------------------------------------|:-------:|
| OarIdleAnimation             | Animation when not using oar            |  "..."  |
| OarRowForwardAnimation       | Animation when moving forward           |  "..."  |
| OarRowReverseAnimation       | Animation when moving backward          |  "..."  |
| MaxMountDistance             | Max distance away that player can mount |   2.5   |


Issues
=====================================
- Player yaw is locked to boat orientation, so player can't use
  tools or interact with things around it while in boat


Credits/Contributions
=====================================
- Cendar: French translation


Dev stuff
=====================================
This is setup for development from VS Code.

1. Download C#/dotnet/VS code plugins: <https://code.visualstudio.com/docs/languages/dotnet>

2. Launch run task in VS code (clr or mono)

3. Build actual release .zip using:  
`dotnet build -c release`