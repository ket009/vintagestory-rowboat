﻿using System;
using Vintagestory.API;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.GameContent;

[assembly: ModInfo( "xrowboat",
    Description = "Rowboat",
    Website     = "",
    Authors     = new []{ "xeth" } )]

namespace XRowboat
{
    public class XRowboatMod : ModSystem
    {
        public static string PATH { get; } = "xrowboat";

        public static string CONFIG_PATH { get; } = "xrowboat_config.json";


        // internal variable names
        internal static string ATTRIBUTE_ROWING = "xrowboatRowing";
        internal static string ATTRIBUTE_ALLOW_FRONT_SEAT_CONTROL = "allowFrontSeatControl";
        internal static string ATTRIBUTE_ALLOW_REAR_SEAT_CONTROL = "allowRearSeatControl";
        internal static string ATTRIBUTE_REQUIRE_OAR = "requireOar";

        public static XRowboatConfig Config = new XRowboatConfig();

        // server
        ICoreServerAPI sapi;
        IServerNetworkChannel serverChannel;

        // client
        ICoreClientAPI capi;
        IClientNetworkChannel clientChannel;


        public override void Start(ICoreAPI api)
        {
            api.RegisterEntity(EntityRowboat.NAME, typeof(EntityRowboat));

            api.RegisterItemClass(ItemRowboat.NAME, typeof(ItemRowboat));
            api.RegisterItemClass(ItemOar.NAME, typeof(ItemOar));

            api.RegisterMountable(EntityRowboatSeat.NAME, EntityRowboatSeat.GetMountable);
            
            // load config
            try
            {
                XRowboatConfig loadedConfig = api.LoadModConfig<XRowboatConfig>(XRowboatMod.CONFIG_PATH);
                if ( loadedConfig != null )
                {
                    XRowboatMod.Config = loadedConfig;
                }
                else
                {
                    api.StoreModConfig(XRowboatMod.Config, CONFIG_PATH);
                }
            }
            catch ( System.Exception e )
            {
                System.Console.WriteLine("{0} Failed to load mod config.", e);
            }
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            capi = api;

            clientChannel =
                api.Network.RegisterChannel("xrowboat")
                .RegisterMessageType(typeof(PacketClientToServerMountRowboat))
                .RegisterMessageType(typeof(PacketServerToClientMountRowboatSuccess))
                .RegisterMessageType(typeof(PacketClientToServerUnmountRowboat))
                .RegisterMessageType(typeof(PacketClientToServerBoatConfigRequest))
                .RegisterMessageType(typeof(PacketServerToClientBoatConfig))
                .SetMessageHandler<PacketServerToClientMountRowboatSuccess>(ClientOnMountSuccessPacket)
                .SetMessageHandler<PacketServerToClientBoatConfig>(ClientOnBoatConfigPacket)
            ;
        }

        public override void StartServerSide(ICoreServerAPI api) {
            sapi = api;

            serverChannel =
                api.Network.RegisterChannel("xrowboat")
                .RegisterMessageType(typeof(PacketClientToServerMountRowboat))
                .RegisterMessageType(typeof(PacketServerToClientMountRowboatSuccess))
                .RegisterMessageType(typeof(PacketClientToServerUnmountRowboat))
                .RegisterMessageType(typeof(PacketClientToServerBoatConfigRequest))
                .RegisterMessageType(typeof(PacketServerToClientBoatConfig))
                .SetMessageHandler<PacketClientToServerMountRowboat>(ServerOnMountPacket)
                .SetMessageHandler<PacketClientToServerUnmountRowboat>(ServerOnUnmountPacket)
                .SetMessageHandler<PacketClientToServerBoatConfigRequest>(ServerOnBoatConfigRequestPacket)
            ;
        }

        public void ClientSendMountPacket(long boatEntityId, RowboatSeat seat)
        {
            clientChannel.SendPacket(new PacketClientToServerMountRowboat()
            {
                BoatEntityId = boatEntityId,
                Seat = (byte) seat,
            });
        }

        public void ClientSendUnmountPacket(long boatEntityId, RowboatSeat seat)
        {
            clientChannel.SendPacket(new PacketClientToServerUnmountRowboat()
            {
                BoatEntityId = boatEntityId,
                Seat = (byte) seat,
            });
        }
        
        private void ServerOnMountPacket(IPlayer fromPlayer, PacketClientToServerMountRowboat packet)
        {
            Entity boatEntitySelected = sapi.World.GetEntityById(packet.BoatEntityId);
            RowboatSeat seat = (RowboatSeat) packet.Seat;

            if ( boatEntitySelected != null && boatEntitySelected is EntityRowboat )
            {
                EntityRowboat boat = (EntityRowboat) boatEntitySelected;
                bool successfulMount = false;

                if ( seat == RowboatSeat.Front )
                {
                    if ( boat.SeatFront.Passenger == null || boat.SeatFront.Passenger?.EntityId == fromPlayer.Entity.EntityId )
                    {
                        successfulMount = fromPlayer.Entity.TryMount(boat.SeatFront);
                    }
                }
                else { // rear
                    if ( boat.SeatRear.Passenger == null || boat.SeatRear.Passenger?.EntityId == fromPlayer.Entity.EntityId )
                    {
                        successfulMount = fromPlayer.Entity.TryMount(boat.SeatRear);
                    }
                }

                if ( successfulMount )
                {
                    // send mount success to player, sends seats + controls config
                    serverChannel.SendPacket(PacketServerToClientMountRowboatSuccess.FromEntitySeatAndSettings(
                        packet.BoatEntityId,
                        seat,
                        XRowboatMod.Config.AllowFrontSeatToControlBoat,
                        XRowboatMod.Config.AllowRearSeatToControlBoat,
                        XRowboatMod.Config.RequireOar
                    ),
                    fromPlayer as IServerPlayer);
                }
            }
        }

        private void ServerOnUnmountPacket(IPlayer fromPlayer, PacketClientToServerUnmountRowboat packet)
        {
            Entity boatEntity = sapi.World.GetEntityById(packet.BoatEntityId);
            RowboatSeat seat = (RowboatSeat) packet.Seat;

            if ( boatEntity != null && boatEntity is EntityRowboat )
            {
                EntityRowboat boat = (EntityRowboat) boatEntity;

                if ( seat == RowboatSeat.Front )
                {
                    if ( boat.SeatFront.Passenger != null || boat.SeatFront.Passenger?.EntityId == fromPlayer.Entity.EntityId )
                    {
                        fromPlayer.Entity.TryUnmount();
                    }
                }
                else { // rear
                    if ( boat.SeatRear.Passenger != null || boat.SeatRear.Passenger?.EntityId == fromPlayer.Entity.EntityId )
                    {
                        fromPlayer.Entity.TryUnmount();
                    }
                }
            }
        }

        private void ClientOnMountSuccessPacket(PacketServerToClientMountRowboatSuccess packet)
        {
            Entity boatEntitySelected = capi.World.GetEntityById(packet.BoatEntityId);

            if ( boatEntitySelected != null && boatEntitySelected is EntityRowboat )
            {
                EntityRowboat boat = (EntityRowboat) boatEntitySelected;
                IPlayer player = capi.World.Player;

                // unpack packet
                PacketServerToClientMountRowboatSuccessUnpacked unpacked = PacketServerToClientMountRowboatSuccessUnpacked.FromPacket(packet);
                RowboatSeat seat = unpacked.Seat;
                bool allowFrontSeatControls = unpacked.AllowFrontSeatControl;
                bool allowRearSeatControls = unpacked.AllowRearSeatControl;
                bool requireOar = unpacked.RequireOar;

                // set boat flags, theres probably better way to sync server settings...
                boat.ClientAllowFrontSeatControl = allowFrontSeatControls;
                boat.ClientAllowRearSeatControl = allowRearSeatControls;
                boat.ClientRequireOar = requireOar;

                bool successfulMount; // for debugging
                if ( seat == RowboatSeat.Front )
                {
                    successfulMount = player.Entity.TryMount(boat.SeatFront);
                }
                else // rear
                {
                    successfulMount = player.Entity.TryMount(boat.SeatRear);
                }
                
                // try to stop swimming/flying animation, does not work
                player.Entity.StopAnimation("swim");
                player.Entity.StopAnimation("swimidle");
                player.Entity.StopAnimation("glide");
            }
        }

        public void ClientSendBoatConfigRequestPacket()
        {
            clientChannel.SendPacket(new PacketClientToServerBoatConfigRequest());
        }

        public void ServerOnBoatConfigRequestPacket(IPlayer fromPlayer, PacketClientToServerBoatConfigRequest packet)
        {
            serverChannel.SendPacket(new PacketServerToClientBoatConfig()
            {
                AllowFrontSeatControls = Config.AllowFrontSeatToControlBoat,
                AllowRearSeatControls = Config.AllowRearSeatToControlBoat,
                RequireOar = Config.RequireOar,
            },
            fromPlayer as IServerPlayer);
        }

        private void ClientOnBoatConfigPacket(PacketServerToClientBoatConfig packet)
        {
            IPlayer player = capi.World.Player;
            if ( player.Entity.MountedOn != null && player.Entity.MountedOn is EntityRowboatSeat )
            {
                EntityRowboat boat = (player.Entity.MountedOn as EntityRowboatSeat).Host;
                boat.ClientAllowFrontSeatControl = packet.AllowFrontSeatControls;
                boat.ClientAllowRearSeatControl = packet.AllowRearSeatControls;
                boat.ClientRequireOar = packet.RequireOar;
            }
        }
    }

    /**
     * Json mod config 
     */
    public class XRowboatConfig
    {
        // ============================================================
        // high level mechanics
        // ============================================================
        // require `oar` item in hand to control rowboat
        public bool RequireOar = true;

        // allow front seat to control boat
        public bool AllowFrontSeatToControlBoat = true;

        // allow rear seat player to also control boat
        // disable => minecraft style boat (only front controls)
        public bool AllowRearSeatToControlBoat = true;

        // ============================================================
        // rowboat speed/control mechanics
        // ============================================================
        // max forward/reverse speed in water
        // for single player and double player rowing
        public double MaxForwardSpeedSingle = 0.15;
        public double MaxReverseSpeedSingle = -0.05;
        public double MaxForwardSpeedDouble = 0.18;
        public double MaxReverseSpeedDouble = -0.1;

        // max forward/reverse speed on land (no dual player setting)
        public double MaxForwardSpeedOnLand = 0.02;
        public double MaxReverseSpeedOnLand = -0.02;

        // forward/reverse acceleration per player (sign should be positive)
        public double ForwardAcceleration = 0.005;
        public double ReverseAcceleration = 0.005;

        // multiplicative water drag deceleration
        public double DragDecelerationFactor = 0.8;
        
        // clamp value before setting speed to 0
        public double ClampSpeedToZeroThreshold = 0.1;

        // turning acceleration per player
        public double TurnAcceleration = 0.002;

        // multiplicative water drag deceleration
        public double DragTurningFactor = 0.6;

        // max rotation rates
        public double MaxAngularVelocitySingle = 0.05;
        public double MaxAngularVelocityDouble = 0.08;

        // ============================================================
        // plugin detailed mechanics
        // ============================================================
        // animation to play when using not using oar
        public string OarIdleAnimation = "holdinglanternrighthand";
        // animation to play when using oar while moving rowboat
        public string OarRowForwardAnimation = "row_forward";
        public string OarRowReverseAnimation = "row_reverse";

        // max block distance for mounting
        public double MaxMountDistance = 2.5;

        // TODO: how many times ticks per sending current controls packet to server
        public int PacketSendPeriod = 2;

        // TODO: how many ticks to wait before starting deceleration
        // must be long enough to wait for next controls packets to come
        public int TicksBeforeDeceleration = 4;

        public int ClientDebugPeriod = 20;
    }
}
