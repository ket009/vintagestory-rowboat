/**
 * Define controls format and networking protocol.
 * Controls byte packet format:
 *      [0000SSFF]
 * F - forward state, 2 bits
 * S - steering state, 2 bits
 * 0 - unused
 */

using System;
using ProtoBuf;

namespace XRowboat {

    public enum RowboatSeat: uint
    {
        Front = 0,
        Rear = 1,
    }

    /**
     * Packet from client -> server requesting mounting a seat of BoatEntityId
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientToServerMountRowboat
    {
        public long BoatEntityId;
        public byte Seat;
    }

    /**
     * Packet from server -> client player indicating their mount attempt
     * of BoatEntityId was successful
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerToClientMountRowboatSuccess
    {
        public long BoatEntityId;
        public byte Payload; // [RequireOar | AllowRearControls | AllowFrontControls | Seat]
    

        public static PacketServerToClientMountRowboatSuccess FromEntitySeatAndSettings(
            long boatEntityId,
            RowboatSeat seat,
            bool allowFrontSeatControl,
            bool allowRearSeatControl,
            bool requireOar
        ) {
            return new PacketServerToClientMountRowboatSuccess()
            {
                BoatEntityId = boatEntityId,
                Payload = (byte) ((uint) seat | ( Convert.ToUInt32(allowFrontSeatControl) << 2) | ( Convert.ToUInt32(allowRearSeatControl) << 3) | ( Convert.ToUInt32(requireOar) << 4))
            };
        }
    }

    // Convenience struct to unpack the packet fields from payload
    public struct PacketServerToClientMountRowboatSuccessUnpacked
    {
        public long EntityId;
        public RowboatSeat Seat;
        public bool AllowFrontSeatControl;
        public bool AllowRearSeatControl;
        public bool RequireOar;

        public static PacketServerToClientMountRowboatSuccessUnpacked FromPacket(PacketServerToClientMountRowboatSuccess packet)
        {
            return new PacketServerToClientMountRowboatSuccessUnpacked()
            {
                EntityId = packet.BoatEntityId,
                Seat = (RowboatSeat) ((uint) packet.Payload & 0b0011),
                AllowFrontSeatControl = (packet.Payload & 0b0100) != 0,
                AllowRearSeatControl = (packet.Payload & 0b1000) != 0,
                RequireOar = (packet.Payload & 0b10000) != 0,
            };
        }
    }
    
    /**
     * Packet from client -> server notifying player
     * unmounted a seat of BoatEntityId.
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientToServerUnmountRowboat {
        public long BoatEntityId;
        public byte Seat;
    }

    // Packet from Server -> Client that an entity successfully mounted a boat entity.
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct PacketServerToClientEntityMountedRowboat
    {
        public long BoatEntityId;
        public long PassengerEntityId;
        public byte Seat;

        public static PacketServerToClientEntityMountedRowboat FromEntityAndSeat(long boatEntityId, long passengerEntityId, RowboatSeat seat)
        {
            return new PacketServerToClientEntityMountedRowboat() {
                BoatEntityId = boatEntityId,
                PassengerEntityId = passengerEntityId,
                Seat = (byte) (seat),
            };
        }
    }

    // Packet from Server -> Client that an entity successfully unmounted a boat entity.
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct PacketServerToClientEntityUnmountedRowboat
    {
        public long BoatEntityId;
        public long PassengerEntityId;
        public byte Seat;

        public static PacketServerToClientEntityUnmountedRowboat FromEntityAndSeat(long boatEntityId, long passengerEntityId, RowboatSeat seat)
        {
            return new PacketServerToClientEntityUnmountedRowboat()
            {
                BoatEntityId = boatEntityId,
                PassengerEntityId = passengerEntityId,
                Seat = (byte) (seat),
            };
        }
    }

    /**
     * Packet from client -> server, player request
     * server boat configuration
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientToServerBoatConfigRequest
    {

    }

    /**
     * Packet from server -> client player syncing
     * server boat configuration
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerToClientBoatConfig
    {
        public bool AllowFrontSeatControls;
        public bool AllowRearSeatControls;
        public bool RequireOar;
    }
}